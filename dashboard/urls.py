from django.urls import path
from . import views


urlpatterns = [
    path('dashboard/', views.index, name='dashboard-index'),
    path('patron/', views.patron, name='dashboard-patron'),
    path('patron/detail/<int:pk>/', views.patron_detail, name='dashboard-patron-detail'),
    path('patron/delete/<int:pk>/', views.patron_delete, name='dashboard-patron-delete'),
    path('item/', views.item, name='dashboard-item'),
    path('order/', views.order, name='dashboard-order'),
]
