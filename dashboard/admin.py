from django.contrib import admin
from .models import Item, ItemType, Manufacturer, Transaction, Patron_Position
from django.contrib.auth.models import Group


# Register your models here.


class ItemAdmin(admin.ModelAdmin):
    list_display = ('model', 'manufacturer_id', 'type_id',)
    list_filter = ['manufacturer_id', 'type_id']



admin.site.unregister(Group)
admin.site.register(Item, ItemAdmin)
admin.site.register(ItemType)
admin.site.register(Manufacturer)
admin.site.register(Transaction)
admin.site.register(Patron_Position)