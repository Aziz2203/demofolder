
from django.db import models


from django.contrib.auth.models import User

# Create your models here.
CONDITION =  (
    ('Broken', 'Broken'),
    ('Working', 'Working'),
    ('Lost', 'Lost'),
)

STATUS =  (
    ('Assigned', 'Assigned'),
    ('Instorage', 'Instorage'),
    ('Sold', 'Sold'),
)



class ItemType(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class Manufacturer(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class Patron_Position(models.Model):
    position_name = models.CharField(max_length=50)

    def __str__(self):
        return self.position_name
    


 

class Item(models.Model):
    inventory_id = models.PositiveIntegerField()
    type_id=models.ForeignKey(ItemType, on_delete=models.CASCADE)
    model = models.CharField(max_length=50)
    manufacturer_id = models.ForeignKey(Manufacturer, on_delete=models.CASCADE)
    serial_number = models.CharField(max_length=50)
    purchase_year = models.DateField(null=True, blank=True)
    condition = models.CharField(max_length=50, choices=CONDITION)
    color = models.CharField(max_length=20)
    status = models.CharField(max_length=50, choices=STATUS)
    create_date = models.DateTimeField(auto_now_add=True)
    create_by = models.ForeignKey(User,  on_delete=models.CASCADE, related_name="item_create")
    update_date = models.DateTimeField(auto_now_add=True)
    update_by = models.ForeignKey(User,  on_delete=models.CASCADE, related_name="item_update")
    config = models.TextField(null=True, blank=True)
    notes = models.TextField(null=True, blank=True)

    class Meta:
        verbose_name_plural = 'Item'

    def __str__(self):
        return f'{self.model}'

class Transaction(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    item_id = models.ForeignKey(Item, on_delete=models.CASCADE, null=True)
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = 'Transaction'
    
    def __str__(self):
        return f'{self.item_id} ordered by {self.user_id}'



