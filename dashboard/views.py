from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from .models import Item, Transaction
from .forms import ItemForm, PatronForm
from django.contrib.auth.models import User
from django.contrib import messages
from django.db.models import Q

# Create your views here.

@login_required
def index(request):
    orders = Transaction.objects.all()
    items = Item.objects.all()
    workers = User.objects.all()
    workers_count = workers.count()
    orders_count = Transaction.objects.all().count()
    products_count = Item.objects.all().count()
    context = {
        'items':items,
        'orders':orders,
        'workers_count':workers_count,
        'orders_count':orders_count,
        'products_count':products_count,
    }
    return render(request, 'dashboard/index.html', context)

@login_required
def patron(request):
    if 'q' in request.GET:
        q = request.GET['q']
        multiple_q = Q(Q(username__icontains=q) | Q(first_name__icontains=q) | Q(last_name__icontains=q) | Q(email__icontains=q))
        workers = User.objects.filter(multiple_q)
    else:
        workers = User.objects.all()
    workers_count = workers.count()
    orders_count = Transaction.objects.all().count()
    products_count = Item.objects.all().count()
    if request.method=='POST':
        form = PatronForm(request.POST)
        if form.is_valid():
            form.save()
            patron_name = form.cleaned_data.get('user_name')
            messages.success(request, f'{patron_name} has been added')
            return redirect('dashboard-patron')
    else:
        form = PatronForm()
    context = {
        'workers':workers,
        'form':form,
        'workers_count':workers_count,
        'orders_count':orders_count,
        'products_count':products_count,
    }
    return render(request, 'dashboard/patron.html', context)

@login_required
def patron_delete(request, pk):
    patron = User.objects.get(id=pk)
    if request.method=="POST":
        patron.delete()
        return redirect('dashboard-patron')
    return render(request, 'dashboard/patron_delete.html')

@login_required
def patron_detail(request, pk):
    workers = User.objects.get(id=pk)
    context = {
        'workers':workers,
    }
    return render(request, 'dashboard/patron_detail.html', context)

@login_required
def item(request):
    item_data = {
        'status':'Instorage'
    }
    if 'q' in request.GET:
        q = request.GET['q']
        multiple_q = Q(Q(model__icontains=q) | Q(serial_number__icontains=q))
        # items = Item.objects.filter(model__icontains=q)
        items = Item.objects.filter(multiple_q)
    else:
        items = Item.objects.all()
    workers_count = User.objects.all().count()
    orders_count = Transaction.objects.all().count()
    products_count = items.count()
    if request.method=='POST':
        form = ItemForm(request.POST)
        if form.is_valid():
            form.save()
            item_name = form.cleaned_data.get('model')
            messages.success(request, f'{item_name} has been added')
            return redirect('dashboard-item')
    else:
        form = ItemForm(initial=item_data)
    context = {
        'items':items,
        'form':form,
        'workers_count':workers_count,
        'orders_count':orders_count,
        'products_count':products_count,

    }
    return render(request, 'dashboard/item.html', context)

@login_required
def order(request):
    # if 'q' in request.GET:
    #     q = request.GET['q']
    #     # multiple_q = Q(Q(user_id__icontains=q))
    #     orders = Transaction.objects.filter(user_id__icontains=q)
    #     # orders = Transaction.objects.filter(multiple_q)
    # else:
    orders = Transaction.objects.all()
    orders_count = orders.count()
    workers_count = User.objects.all().count()
    products_count = Item.objects.all().count()
    context = {
        'orders':orders,
        'workers_count':workers_count,
        'orders_count':orders_count,
        'products_count':products_count,
    }
    return render(request, 'dashboard/order.html', context)
