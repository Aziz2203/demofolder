from django import forms
from user.models import Patron
from .models import Item

class ItemForm(forms.ModelForm):
    class Meta:
        model = Item
        fields = ['inventory_id', 'type_id', 'model', 'manufacturer_id', 'serial_number', 'purchase_year', 'condition', 'color', 'status', 'create_by', 'update_by', 'config', 'notes']

class PatronForm(forms.ModelForm):
    class Meta:
        model = Patron
        fields = ['staff', 'type', 'first_name', 'last_name', 'department', 'position', 'user_name', 'profile', 'image']
