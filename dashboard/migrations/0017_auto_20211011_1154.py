# Generated by Django 3.1.13 on 2021-10-11 06:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0016_auto_20211011_1151'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='config',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='item',
            name='notes',
            field=models.TextField(blank=True, null=True),
        ),
    ]
