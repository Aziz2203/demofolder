# Generated by Django 3.1.13 on 2021-10-11 06:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0013_auto_20211011_1146'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='purchase_year',
            field=models.DateField(blank=True),
        ),
    ]
