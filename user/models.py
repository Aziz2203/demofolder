from django.db import models
from django.contrib.auth.models import User
from dashboard.models import Patron_Position

# Create your models here.

PATRON_TYPE =(
    ('Student', 'Student'),
    ('Staff', 'Staff'),
    ('Room', 'Room'),
)

DEPARTMENT =(
    ('Elementary', 'Elementary'),
    ('Secondary', 'Secondary'),
    ('Administrative', 'Administrative'),
)

class Patron(models.Model):    
    staff = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    type = models.CharField(max_length=50, choices=PATRON_TYPE, null=True)
    first_name = models.CharField(max_length=30, null=True, blank=True)
    last_name = models.CharField(max_length=30, null=True, blank=True)
    department = models.CharField(max_length=30, choices=DEPARTMENT, null=True, blank=True)
    position = models.ForeignKey(Patron_Position, on_delete=models.CASCADE, null=True, blank=True)
    user_name = models.CharField(max_length=50, null=True, blank=True)
    profile = models.TextField(null=True, blank=True)
    image = models.ImageField(default='avatar.jpg', upload_to='Profile_Images', null=True, blank=True)


    def __str__(self):
        return f'{self.staff.username}-profile'
