from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from .forms import CreateUserForm, UserUpdateForm, PatronUpdateForm
from django.contrib import messages

# Create your views here.
def register(request):
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Account has been created for {username}. Continue to Login ')
            return redirect('user-login')
    else: 
        form = CreateUserForm()
    context = {
        'form':form,

    }
    return render(request, 'user/register.html', context)

def profile(request):
    return render(request, 'user/profile.html')

def profile_update(request):
    if request.method=='POST':
        user_form = UserUpdateForm(request.POST, instance=request.user)
        patron_form= PatronUpdateForm(request.POST, request.FILES, instance=request.user.patron)
        if user_form.is_valid() and patron_form.is_valid():
            user_form.save()
            patron_form.save()
            return redirect('user-profile')
    else:
        user_form = UserUpdateForm(instance=request.user)
        patron_form = patron_form= PatronUpdateForm(instance=request.user.patron)
    context = {
        'user_form':user_form,
        'patron_form':patron_form,
    }
    return render(request, 'user/profile_update.html', context)